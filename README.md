# TiKV Client
This is a simple test client for the [TiKV](https://tikv.org) database. It's written in Rust
and uses the official [Rust client library](https://github.com/tikv/client-rust).
