use failure::Fail;
use log::LevelFilter;

/// Macro for creating argument parser.
#[macro_export]
macro_rules! arg_parser {
    ($name:expr) => {{
        use clap::{App, Arg};
        App::new($name)
            .version(env!("CARGO_PKG_VERSION"))
            .author(env!("CARGO_PKG_AUTHORS"))
            .about(env!("CARGO_PKG_DESCRIPTION"))
            .arg(
                Arg::with_name("verbose")
                    .short("v")
                    .multiple(true)
                    .help("Increase verbosity"),
            )
    }};
}

/// Our error type.
#[derive(Fail, Debug)]
pub enum TiKvClientError {
    /// An unknown error occurred.
    #[fail(display = "{}", _0)]
    UnknownError(String),

    /// A SetLoggerError occurred.
    #[fail(display = "{}", _0)]
    SetLoggerError(#[fail(cause)] log::SetLoggerError),
}

impl From<log::SetLoggerError> for TiKvClientError {
    fn from(err: log::SetLoggerError) -> TiKvClientError {
        TiKvClientError::SetLoggerError(err)
    }
}

/// Result type.
pub type Result<T> = std::result::Result<T, TiKvClientError>;

/// Set up logging subsystem.
pub fn setup_logging(verbosity: u64) -> Result<()> {
    let level = match verbosity {
        0 => {
            let level_str = std::env::var("RUST_LOG")
                .unwrap_or("info".to_string())
                .to_lowercase();
            match level_str.as_str() {
                "info" => LevelFilter::Info,
                "debug" => LevelFilter::Debug,
                "trace" => LevelFilter::Trace,
                "error" => LevelFilter::Error,
                "warn" => LevelFilter::Warn,
                "off" => LevelFilter::Off,
                _ => {
                    return Err(TiKvClientError::UnknownError(format!(
                        "unrecognized log level: '{}'",
                        level_str
                    )));
                }
            }
        }
        1 => LevelFilter::Debug,
        _ => LevelFilter::Trace,
    };
    fern::Dispatch::new()
        .level(level)
        .format(|out, message, record| {
            out.finish(format_args!(
                "{}[{}][{}] {}",
                chrono::Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
                record.target(),
                record.level(),
                message
            ))
        })
        .chain(std::io::stderr())
        .apply()?;
    Ok(())
}
